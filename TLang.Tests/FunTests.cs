﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TLang.Values;

namespace TLang.Tests
{
    [TestClass]
    public class FunTests
    {
        [TestMethod]
        public void FunTest1()
        {
            string text = @"
(define f (fun (x y) (+ x y)))
(f 1 2)";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(3, value.Value);
        }

        [TestMethod]
        public void FunTest2()
        {
            string text = @"
(fun f ([x Int] [y Int] [-> Int]) (+ x y))
(f 1 2)";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(3, value.Value);
        }

        [TestMethod]
        public void FunTest3()
        {
            string text = @"
(define f (fun ([x Int :default 1] [y Int]) (+ x y)))
(f :y 2)";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(3, value.Value);
        }

        [TestMethod]
        public void FunTest4()
        {
            string text = @"
(define f (fun ([x Int :default 1] [y Int] [-> Int]) (+ x y)))
(f :y 2)";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(3, value.Value);
        }

        [TestMethod]
        public void FunTest5()
        {
            string text = @"
((fun (z x y) (+ x y)) 0 1 2)";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(3, value.Value);
        }

        [TestMethod]
        public void FunTest6()
        {
            string text = @"
(fun f (x y) (+ x y))
(f 1 2)";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(3, value.Value);
        }

        [TestMethod]
        public void FunTest7()
        {
            string text = @"
(fun f ([x Int :default 1] [y Int] [-> Int]) (+ x y))
(f :y 2)";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(3, value.Value);
        }

        [TestMethod]
        public void FunTest8()
        {
            string text = @"
(define a 1)
(fun f () 
        (define a 2) 
        (g)
)
(fun g () 
    a
)
(f)";

            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(1, value.Value);
        }

        [TestMethod]
        public void FunTest9()
        {
            string text = @"
--求最大公约数
(fun gcd (p q)
    (if (= q 0)
        p
        (seq 
            (define r (% p q))
            (gcd q r))))
[(= (gcd 1 2) 1)
(= (gcd 2 3) 1)
(= (gcd 2 4) 2)]";

            VectorValue value = (VectorValue)Interpreter.InterpText(text);
            for (int i = 0; i < value.Count; i++)
            {
                Assert.IsTrue(((BoolValue)value[i]).Value);
            }
        }
    }
}
