﻿--下面是用tlang书写的一段合约逻辑
--这个合约的逻辑是：要么知道我的birthDay是2017-06-23 要么 给出pubKey和用对应的私钥对你的pubKeyHash的签名。
--由合约调用者传入的三个参数赋值下面三个变量。
--合约定义方定义逻辑，合约调用方定义数据。

--合约调用方提供的数据：
(define birthDay "{birthDay}")
(define pubKey "{pubKey}")
(define sign "{sign}")

--上面的键值对来自合约调用者，数据是键值对，执行时直接把键值对加入作用域不需要拼代码从而没有注入代码的可能。
--下面三行是智能合约的逻辑，这三行是死的不会被调用者注入代码。
--合约定义方定义的逻辑：
(define pubKeyHash="8658cefd0c1cf0ff294d14a66278f5a5490a1f8e")
(define myBirthDayHash="d4a0f6c5b4bcbf2f5830eabed3daa7304fb794d6")
(or 
	(= (hash160 birthDay) myBirthDayHash) 
	(and 
		(= (hash160 pubKey) pubKeyHash) 
		(= (decrypt pubKey sign) pubKeyHash)))

--and = hash160 or decrypt是系统函数。
--如果提供checksig系统函数的话(= (decrypt pubKey sign) pubKeyHash)也可以是(checksig pubKey sign pubKeyHash)。
--ps:话说，这个案例不好，因为生日可以被枚举出来，从今天往前推100年挨着计算每一天的hash然后跟myBirthDayHash比对的话就能取走被这个合约锁定的资产了。