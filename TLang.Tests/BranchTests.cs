﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TLang.Values;

namespace TLang.Tests
{
    [TestClass]
    public class BranchTests
    {
        [TestMethod]
        public void IfTest()
        {
            string text = @"
(if (< 3 2) 1 2)";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(2, value.Value);
        }

        [TestMethod]
        public void IfTest1()
        {
            string text = @"
(if (> 3 2) 1 2)";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(1, value.Value);
        }

        [TestMethod]
        public void IfTest2()
        {
            string text = @"
(define x 1)
(if (< x 1) (set! x 2) (set! x 3))
x
";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(3, value.Value);
        }

        [TestMethod]
        public void IfTest3()
        {
            string text = @"
(define x 1)
(define y (set! x 0))
[y (if (< x 1) (set! x 2) (set! x 3))]
";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);
            Assert.AreEqual(0, ((IntValue)value[0]).Value);
            Assert.AreEqual(2, ((IntValue)value[1]).Value);
        }

        [TestMethod]
        [ExpectedException(typeof(GeneralError))]
        public void IfTest4()
        {
            string text = @"
(if 1 2 3)
";
            Value value = Interpreter.InterpText(text);
            Assert.AreEqual(VoidValue.Void, value);
        }

        [TestMethod]
        public void IfTest5()
        {
            string text = @"
(if (= 1 2) 2 
(if (= 1 1) 1 
(if (= 2 2) 2 void)))
";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(1, value.Value);
        }
        
        [TestMethod]
        public void CondTest1()
        {
            string text = @"
(cond 
    [(= 1 2) 1]
    [(= 1 1) 2]
    [(< 1 2) 3])";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(2, value.Value);
        }

        [TestMethod]
        public void CondTest2()
        {
            string text = @"
(cond 
    [(= 1 2) 1]
    [(= 1 3) 2]
    [else 3])";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(3, value.Value);
        }

        [TestMethod]
        public void CondTest3()
        {
            string text = @"
(cond 
    ((= 1 2) 1)
    ((= 1 1) 2)
    ((< 1 2) 3))";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(2, value.Value);
        }

        [TestMethod]
        public void CaseTest1()
        {
            string text = @"
(case 2 
    (1 1)
    (2 2)
    (3 3))";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(2, value.Value);
        }

        [TestMethod]
        public void CaseTest2()
        {
            string text = @"
(define a ""abc"")
(case a 
    [""test"" 1]
    [""abc"" 2]
    [else 3])";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(2, value.Value);
        }

        [TestMethod]
        public void CaseTest3()
        {
            string text = @"
(define a ""abc"")
(case a 
    (""test"" 1)
    (""aaa"" 2)
    (else 3))";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(3, value.Value);
        }

        [TestMethod]
        public void CaseTest4()
        {
            string text = @"
(define a ""abc"")
(case (typeof a) 
    (Int 1)
    (String 2)
    (else 3))";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(2, value.Value);
        }
    }
}
