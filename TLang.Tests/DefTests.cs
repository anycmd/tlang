﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TLang.Values;

namespace TLang.Tests
{
    [TestClass]
    public class DefTests
    {
        [TestMethod]
        [ExpectedException(typeof(GeneralError))]
        public void DefTest1()
        {
            string text = @"
(define x 1)
(define x 2)";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(2, value.Value);
        }

        [TestMethod]
        [ExpectedException(typeof(GeneralError))]
        public void DefTest2()
        {
            string text = @"
(define [x y] [1 2])
(define [x y] [1 2])";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(2, value.Value);
        }

        [TestMethod]
        public void DefTest3()
        {
            string text = @"
(define {:x a :y b} {:x 1 :y 2})
b";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(2, value.Value);
        }

        [TestMethod]
        [ExpectedException(typeof(GeneralError))]
        public void DefTest4()
        {
            string text = @"
(define {:x a :y b} {:x 1 :y 2})
(define {:x a :y b} {:x 1 :y 2})";
            IntValue value = (IntValue)Interpreter.InterpText(text);
        }

        [TestMethod]
        public void DefTest5()
        {
            string text = @"
(define {:x a :y b} {:x 1 :y 2})
(define f (fun (x y) (seq (define a (+ y 1)) (+ (+ x a) b))))
(f 1 2)";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(6, value.Value);
        }

        [TestMethod]
        public void DefTest6()
        {
            string text = @"
(define {:x a :y b} {:x 1 :y 2})
(define f (fun (x y) (define a (+ y 1)) (+ (+ x a) b)))
(f 1 2)";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(6, value.Value);
        }
    }
}
