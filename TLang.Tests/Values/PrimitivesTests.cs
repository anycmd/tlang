﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TLang.Values;

namespace TLang.Tests.Values
{
    [TestClass]
    public class PrimitivesTests
    {
        [TestMethod]
        public void PowTest1()
        {
            string text = @"
[(^ 2 2) (^ 2 2.5) (^ 2.5 2) (^ 2.5 2.5)]";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);
            Assert.AreEqual(Math.Pow(2, 2), ((FloatValue)value[0]).Value);
            Assert.AreEqual(Math.Pow(2, 2.5), ((FloatValue)value[1]).Value);
            Assert.AreEqual(Math.Pow(2.5, 2), ((FloatValue)value[2]).Value);
            Assert.AreEqual(Math.Pow(2.5, 2.5), ((FloatValue)value[3]).Value);
        }

        [TestMethod]
        public void LenTest1()
        {
            string text = @"
(define a [1 2 3])
(define b {:a 1 :b 2})
[   (len a) 
    (len b) 
    (len [1 2 3]) 
    (len {:a 1 :b 2}) 
    (len []) 
    (len {})
    (len ""test"")
]";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);
            Assert.AreEqual(3, ((IntValue)value[0]).Value);
            Assert.AreEqual(2, ((IntValue)value[1]).Value);
            Assert.AreEqual(3, ((IntValue)value[2]).Value);
            Assert.AreEqual(2, ((IntValue)value[3]).Value);
            Assert.AreEqual(0, ((IntValue)value[4]).Value);
            Assert.AreEqual(0, ((IntValue)value[5]).Value);
            Assert.AreEqual("test".Length, ((IntValue)value[6]).Value);
        }

        [TestMethod]
        public void IncTest1()
        {
            string text = @"
(define x 1)
(define a [1 2 3])
(define b {:x 1 :y 2})
[
    (inc! 1)
    (inc! 1 2)
    (dec! 2)
    (dec! 2 2)
    (inc! x)
    x
    (inc! a.1)
    a.1
    (inc! b.x)
    b.x
]";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);
            Assert.AreEqual(2, ((IntValue)value[0]).Value);
            Assert.AreEqual(3, ((IntValue)value[1]).Value);
            Assert.AreEqual(1, ((IntValue)value[2]).Value);
            Assert.AreEqual(0, ((IntValue)value[3]).Value);
            Assert.AreEqual(2, ((IntValue)value[4]).Value);
            Assert.AreEqual(2, ((IntValue)value[5]).Value);
            Assert.AreEqual(3, ((IntValue)value[6]).Value);
            Assert.AreEqual(3, ((IntValue)value[7]).Value);
            Assert.AreEqual(2, ((IntValue)value[8]).Value);
            Assert.AreEqual(2, ((IntValue)value[9]).Value);
        }

        [TestMethod]
        public void TypeofTest()
        {
            string text = @"
(define x ""test"")
[(= (typeof 1) Int)
(= (typeof 1.1) Float)
(= (typeof true) Bool)
(= (typeof x) String)]";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);
            foreach (var item in value)
            {
                Assert.IsTrue(((BoolValue)item).Value);
            }
        }
    }
}
