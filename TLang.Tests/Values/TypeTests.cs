﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TLang.Values;

namespace TLang.Tests.Values
{
    [TestClass]
    public class TypeTests
    {
        [TestMethod]
        public void TypeTest1()
        {
            IntValue value1 = new IntValue(1);
            FloatValue value2 = new FloatValue(1.1);
            BoolValue value3 = BoolValue.False;
            VoidValue value4 = VoidValue.Void;
            StringValue value5 = new StringValue("123");
            VectorValue value6 = (VectorValue)Interpreter.InterpText("[1 2 3]");
            RecordType value7 = (RecordType)Interpreter.InterpText("{:x 1 :y 2}");
            Closure value8 = (Closure)Interpreter.InterpText("(fun (x) (+ x 1))");

            Assert.AreEqual(TType.Int, value1.Type);
            Assert.AreEqual(TType.Int.Type, value1.Type.Type);
            Assert.AreEqual(TType.Float, value2.Type);
            Assert.AreEqual(TType.Float.Type, value2.Type.Type);
            Assert.AreEqual(TType.Bool, value3.Type);
            Assert.AreEqual(TType.Bool.Type, value3.Type.Type);
            Assert.AreEqual(TType.Void, value4.Type);
            Assert.AreEqual(TType.Void.Type, value4.Type.Type);
            Assert.AreEqual(TType.String, value5.Type);
            Assert.AreEqual(TType.String.Type, value5.Type.Type);
            Assert.AreEqual(TType.Vector, value6.Type);
            Assert.AreEqual(TType.Vector.Type, value6.Type.Type);
            Assert.AreEqual(TType.Record.Type, value7.Type);// 
            Assert.AreEqual(TType.Record.Type, value7.Type.Type);
            Assert.AreEqual(TType.Fun, value8.Type);
            Assert.AreEqual(TType.Fun.Type, value8.Type.Type);


            Assert.AreEqual(TType.Int.Type, TType.Int.Type.Type);
            Assert.AreEqual(TType.Int.Type, TType.Int.Type.Type.Type);
            Assert.AreEqual(TType.Float.Type, TType.Float.Type.Type);
            Assert.AreEqual(TType.Float.Type, TType.Float.Type.Type.Type);
            Assert.AreEqual(TType.Bool.Type, TType.Bool.Type.Type);
            Assert.AreEqual(TType.Bool.Type, TType.Bool.Type.Type.Type);
            Assert.AreEqual(TType.Any.Type, TType.Any.Type.Type);
            Assert.AreEqual(TType.Any.Type, TType.Any.Type.Type.Type);
            Assert.AreEqual(TType.Void.Type, TType.Void.Type.Type);
            Assert.AreEqual(TType.Void.Type, TType.Void.Type.Type.Type);
            Assert.AreEqual(TType.Vector.Type, TType.Vector.Type.Type);
            Assert.AreEqual(TType.Vector.Type, TType.Vector.Type.Type.Type);
            Assert.AreEqual(TType.String.Type, TType.String.Type.Type);
            Assert.AreEqual(TType.String.Type, TType.String.Type.Type.Type);
            Assert.AreEqual(TType.Fun.Type, TType.Fun.Type.Type);
            Assert.AreEqual(TType.Fun.Type, TType.Fun.Type.Type.Type);
            Assert.AreEqual(TType.Record.Type, TType.Record.Type.Type);
            Assert.AreEqual(TType.Record.Type, TType.Record.Type.Type.Type);
        }
    }
}
