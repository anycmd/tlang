using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.IO;
using TLang.Values;
using TLang.Parsers;

namespace TLang.Tests
{

    [TestClass]
    public class Tests
    {
        private static string GetFilePath(string name)
        {
            string file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "../../../tests/" + name);

            return file;
        }

        [TestMethod]
        public void MoreTest()
        {
            VectorValue value = (VectorValue)Interpreter.InterpFile(GetFilePath("moreTest.t"));
            for (int i = 0; i < value.Count; i++)
            {
                Assert.AreEqual(true, ((BoolValue)value[i]).Value);
            }
        }

        [TestMethod]
        public void NotEqTest()
        {
            string text = @"
(and (not (= 1 2)) (not (> 1 2)))";

            BoolValue value = (BoolValue)Interpreter.InterpText(text);
            Assert.AreEqual(true, value.Value);
        }

        [TestMethod]
        public void NotEqTest1()
        {
            string text = @"
(!= 1 2)";

            BoolValue value = (BoolValue)Interpreter.InterpText(text);
            Assert.AreEqual(true, value.Value);
        }

        [TestMethod]
        public void NotTest()
        {
            string text = "(not false)";

            BoolValue value = (BoolValue)Interpreter.InterpText(text);
            Assert.AreEqual(true, value.Value);

            text = "(not true)";

            value = (BoolValue)Interpreter.InterpText(text);
            Assert.AreEqual(false, value.Value);
        }

        [TestMethod]
        public void AndTest1()
        {
            string text = @"
(and (= 1 1) (= 2 2))";

            BoolValue value = (BoolValue)Interpreter.InterpText(text);
            Assert.AreEqual(true, value.Value);
        }

        [TestMethod]
        public void AndTest2()
        {
            string text = @"
(and (= 1 1) (= 1 2))";

            BoolValue value = (BoolValue)Interpreter.InterpText(text);
            Assert.AreEqual(false, value.Value);
        }

        [TestMethod]
        public void AndTest3()
        {
            string text = @"
(and (= 1 2) (= 1 2))";

            BoolValue value = (BoolValue)Interpreter.InterpText(text);
            Assert.AreEqual(false, value.Value);
        }

        [TestMethod]
        public void AndTest4()
        {
            string text = @"
(and (= 1 2) (= 1 1))";

            BoolValue value = (BoolValue)Interpreter.InterpText(text);
            Assert.AreEqual(false, value.Value);
        }

        [TestMethod]
        public void OrTest1()
        {
            string text = @"
(or true true)";

            BoolValue value = (BoolValue)Interpreter.InterpText(text);
            Assert.AreEqual(true, value.Value);
        }

        [TestMethod]
        public void OrTest2()
        {
            string text = @"
(or false true)";

            BoolValue value = (BoolValue)Interpreter.InterpText(text);
            Assert.AreEqual(true, value.Value);
        }

        [TestMethod]
        public void OrTest3()
        {
            string text = @"
(or true false)";

            BoolValue value = (BoolValue)Interpreter.InterpText(text);
            Assert.AreEqual(true, value.Value);
        }

        [TestMethod]
        public void OrTest4()
        {
            string text = @"
(or false false)";

            BoolValue value = (BoolValue)Interpreter.InterpText(text);
            Assert.AreEqual(false, value.Value);
        }

        [TestMethod]
        public void AddTest1()
        {
            string text = @"
(+ 1 1)";

            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(2, value.Value);

            text = @"
(define n (+ 1 1)) n";
            value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(2, value.Value);
        }

        [TestMethod]
        public void AddTest2()
        {
            string text = @"
(+ 1 ""2"")";

            StringValue value = (StringValue)Interpreter.InterpText(text);
            Assert.AreEqual("12", value.Value);

            text = @"
(+ ""2"" 1)";
            value = (StringValue)Interpreter.InterpText(text);
            Assert.AreEqual("21", value.Value);

            text = @"
(+ ""a"" ""b"")";
            value = (StringValue)Interpreter.InterpText(text);
            Assert.AreEqual("ab", value.Value);
        }

        [TestMethod]
        public void SubTest1()
        {
            string text = @"
(- 3 1)";

            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(2, value.Value);
        }

        [TestMethod]
        public void DivTest1()
        {
            string text = @"
(/ 3 2)";

            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(1, value.Value);
        }

        [TestMethod]
        public void DivTest2()
        {
            string text = @"
(/ 3 2.0)";

            FloatValue value = (FloatValue)Interpreter.InterpText(text);
            Assert.AreEqual(1.5, value.Value);
        }

        [TestMethod]
        public void GtTest1()
        {
            string text = @"
(> 3 2.0)";

            BoolValue value = (BoolValue)Interpreter.InterpText(text);
            Assert.AreEqual(true, value.Value);

            text = @"
(> 3 4)";

            value = (BoolValue)Interpreter.InterpText(text);
            Assert.AreEqual(false, value.Value);
        }

        [TestMethod]
        public void GtETest1()
        {
            string text = @"
(>= 3 2.0)";

            BoolValue value = (BoolValue)Interpreter.InterpText(text);
            Assert.AreEqual(true, value.Value);

            text = @"
(>= 3 4)";

            value = (BoolValue)Interpreter.InterpText(text);
            Assert.AreEqual(false, value.Value);
        }

        [TestMethod]
        public void LtTest1()
        {
            string text = @"
(< 3 2.0)";

            BoolValue value = (BoolValue)Interpreter.InterpText(text);
            Assert.AreEqual(false, value.Value);

            text = @"
(< 3 4)";

            value = (BoolValue)Interpreter.InterpText(text);
            Assert.AreEqual(true, value.Value);
        }

        [TestMethod]
        public void LtETest1()
        {
            string text = @"
(<= 3 2.0)";

            BoolValue value = (BoolValue)Interpreter.InterpText(text);
            Assert.AreEqual(false, value.Value);

            text = @"
(<= 3 4)";

            value = (BoolValue)Interpreter.InterpText(text);
            Assert.AreEqual(true, value.Value);
        }

        [TestMethod]
        public void MultTest1()
        {
            string text = @"
(* 3 2.0)";

            FloatValue value = (FloatValue)Interpreter.InterpText(text);
            Assert.AreEqual(6.0, value.Value);

            text = @"
(* 3 4)";

            IntValue value1 = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(12, value1.Value);
        }

        [TestMethod]
        public void PrintTest()
        {
            string text = "(print \"test\")";

            Value value1 = Interpreter.InterpText(text);
        }

        [TestMethod]
        public void ExprTest()
        {
            string text = @"
(define 你好 ""世界"")                    -- unicode
(print 你好)

(print (*(+ 2 3) 4))
(print (+ 2 3))
(print 42 ""ok"" true)";
            Value value = Interpreter.InterpText(text);
        }

        [TestMethod]
        public void Function1Test()
        {
            string text = @"
--- functions may be called with either all positional arguments or all keyword
--- arguments, but not a mixture. This provides convenience but avoids
--- confusions.


(define f
  (fun (x y)
    (- x y)))


[(f 3 2)
(f :x 3 :y 2)
(f :y 3 :x 2)]";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);
            Assert.AreEqual(1, ((IntValue)value[0]).Value);
            Assert.AreEqual(1, ((IntValue)value[1]).Value);
            Assert.AreEqual(-1, ((IntValue)value[2]).Value);
        }

        [TestMethod]
        public void Recursion_directTest()
        {
            string text = @"
(define fact
  (fun ([x Int] [-> Int])
    (if (= x 0) 1 (* x (fact (- x 1))))))

(fact 5)                                -- 120";
            Value value = Interpreter.InterpText(text);

            Assert.AreEqual("120", value.ToString());
        }

        [TestMethod]
        public void Recursion_mutualTest()
        {
            string text = @"
(define even
  (fun ([x Int] [-> Bool])
    (if (= x 0)
        true
        (if (= x 1)
            false
            (odd (- x 1))))))

(define odd
  (fun ([x Int] [-> Bool])
    (if (= x 0)
        false
        (if (= x 1)
            true
            (even (- x 1))))))

(print (even 10))                       -- true
(print (even 11))                       -- false
(print (odd 10))                        -- false
(print (odd 11))                        -- true";
            Value value = Interpreter.InterpText(text);
        }

        [TestMethod]
        public void ArrayTest()
        {
            string text = @"
-- empty array

(define a [])
a";
            Value value = Interpreter.InterpText(text);
            Assert.AreEqual("[]", value.ToString());

            text = "[1 2 3 4 5]";
            value = Interpreter.InterpText(text);
            var list = ((VectorValue)value).Cast<IntValue>().ToArray();

            Assert.AreEqual(1, list[0].Value);
            Assert.AreEqual(2, list[1].Value);
            Assert.AreEqual(3, list[2].Value);
            Assert.AreEqual(4, list[3].Value);
            Assert.AreEqual(5, list[4].Value);


            text = "[true false]";
            value = Interpreter.InterpText(text);
            VectorValue v = (VectorValue)value;

            Assert.AreEqual(true, ((BoolValue)v[0]).Value);
            Assert.AreEqual(false, ((BoolValue)v[1]).Value);
        }

        [TestMethod]
        public void ArrayTest1()
        {
            string text = @"
[2 3 4]
(define aa [1 2 3])
(set! aa.0 10)
aa";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);

            Assert.AreEqual(10, ((IntValue)value[0]).Value);
        }

        [TestMethod]
        public void RecordSetTest()
        {
            string text = @"
(define bb {:x 1 :y 2})
(set! bb.x 10)
bb";
            RecordType value = (RecordType)Interpreter.InterpText(text);

            Assert.AreEqual(10, ((IntValue)value.Properties.Lookup("x")).Value);
        }

        [TestMethod]
        public void RecordSetTest1()
        {
            string text = @"
(define bb {:x [1 2 3] :y 2})
(define aa bb.x.1)
aa";
            IntValue value = (IntValue)Interpreter.InterpText(text);

            Assert.AreEqual(2, value.Value);
        }

        [TestMethod]
        public void RecordSetTest2()
        {
            string text = @"
(define bb {:x [1 2 3] :y 2})
(set! bb.x.1 10)
bb.x.1";
            IntValue value = (IntValue)Interpreter.InterpText(text);

            Assert.AreEqual(10, value.Value);
        }

        [TestMethod]
        public void RecordFunTest()
        {
            string text = @"
(define bb {:x 1 :y 2 :z (fun (x) x)})
(bb.z 10)";
            IntValue value = (IntValue)Interpreter.InterpText(text);

            Assert.AreEqual(10, value.Value);
        }

        [TestMethod]
        public void DefineTest()
        {
            string text = @"
(define [x :Int y :Int] [1 2])
(define z :Float 1.0)
[x y z]";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);

            Assert.AreEqual(1, ((IntValue)value[0]).Value);
            Assert.AreEqual(2, ((IntValue)value[1]).Value);
            Assert.AreEqual(1.0, ((FloatValue)value[2]).Value);
        }

        [TestMethod]
        public void DefineTest1()
        {
            string text = @"
(define [x y] [1 2])
[x y]";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);

            Assert.AreEqual(1, ((IntValue)value[0]).Value);
            Assert.AreEqual(2, ((IntValue)value[1]).Value);
        }

        [TestMethod]
        public void DefineTest2()
        {
            string text = @"
(define {:a x :b y} {:a 1 :b 2})
[x y]";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);

            Assert.AreEqual(1, ((IntValue)value[0]).Value);
            Assert.AreEqual(2, ((IntValue)value[1]).Value);
        }

        [TestMethod]
        public void DefineTest3()
        {
            string text = @"
(define
  {
    :c xx
    :d [uu vv]
  }
  {
   :d [8 9]
   :c 7
  }
)

[xx uu vv]";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);

            Assert.AreEqual(7, ((IntValue)value[0]).Value);
            Assert.AreEqual(8, ((IntValue)value[1]).Value);
            Assert.AreEqual(9, ((IntValue)value[2]).Value);
        }

        [TestMethod]
        public void DefineTest4()
        {
            string text = @"
(define arr [1 2 3])
(define [t1 t2 t3] arr)
[t1 t2 t3]";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);

            Assert.AreEqual(1, ((IntValue)value[0]).Value);
            Assert.AreEqual(2, ((IntValue)value[1]).Value);
            Assert.AreEqual(3, ((IntValue)value[2]).Value);
        }

        [TestMethod]
        public void DefineTest5()
        {
            string text = @"
(define [u1 [v1 v2] u2] [2 [3 5] 7])
[u1 u2 v1 v2]";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);

            Assert.AreEqual(2, ((IntValue)value[0]).Value);
            Assert.AreEqual(7, ((IntValue)value[1]).Value);
            Assert.AreEqual(3, ((IntValue)value[2]).Value);
            Assert.AreEqual(5, ((IntValue)value[3]).Value);
        }

        [TestMethod]
        public void DefineTest6()
        {
            string text = @"
(define f (fun (x) (+ x 1)))
(f 1)";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(2, value.Value);
        }

        [TestMethod]
        public void DefineTest7()
        {
            string text = @"
(define g :Int 1)
g";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(1, value.Value);
        }

        [TestMethod]
        public void DefineTest8()
        {
            string text = @"
(define {:e a1 :f b1} {:e 10 :f 20})
[a1 b1]
";
            Value value = Interpreter.InterpText(text);
        }

        [TestMethod]
        public void DefineTest9()
        {
            string text = @"
(define a 1)
(define b 2)
(define x {:e a :f b})
x
";
            RecordType value = (RecordType)Interpreter.InterpText(text);
            Assert.AreEqual(1, ((IntValue)value.Properties.LookupLocal("e")).Value);
            Assert.AreEqual(2, ((IntValue)value.Properties.LookupLocal("f")).Value);
        }

        [TestMethod]
        public void DefineTest10()
        {
            string text = @"
(define n-0-to-9 [0 1 2 3 4 5 6 7 8 9])
n-0-to-9";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);
            Assert.AreEqual(0, ((IntValue)value[0]).Value);
            Assert.AreEqual(1, ((IntValue)value[1]).Value);
            Assert.AreEqual(9, ((IntValue)value[9]).Value);
        }

        [TestMethod]
        public void DefineTest11()
        {
            string text = @"
(define ~0-to-9 [0 1 2 3 4 5 6 7 8 9])
(define !0-to-9 [0 1 2 3 4 5 6 7 8 9])
(define #0-to-9 [0 1 2 3 4 5 6 7 8 9])
(define ^0-to-9 [0 1 2 3 4 5 6 7 8 9])
(define =0-to-9 [0 1 2 3 4 5 6 7 8 9])
(define |0-to-9 [0 1 2 3 4 5 6 7 8 9])
(define ,0-to-9 [0 1 2 3 4 5 6 7 8 9])
(define <0-to-9 [0 1 2 3 4 5 6 7 8 9])
(define >0-to-9 [0 1 2 3 4 5 6 7 8 9])
(define ?0-to-9 [0 1 2 3 4 5 6 7 8 9])
(define /0-to-9 [0 1 2 3 4 5 6 7 8 9])
(define *0-to-9 [0 1 2 3 4 5 6 7 8 9])
(define _0-to-9 [0 1 2 3 4 5 6 7 8 9])
(define %0-to-9 [0 1 2 3 4 5 6 7 8 9])
(define @0-to-9 [0 1 2 3 4 5 6 7 8 9])
";
            Value value = Interpreter.InterpText(text);
        }

        [TestMethod]
        [ExpectedException(typeof(ParserException))]
        public void DefineTest12()
        {
            string text = @"
(define 0-to-9 [0 1 2 3 4 5 6 7 8 9])";// 变量不能以数字开头
            Value value = Interpreter.InterpText(text);
        }

        [TestMethod]
        public void DefineTest13()
        {
            string text = @"
-- 支持中文变量名
(define 你好 1)
你好";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(1, value.Value);
        }

        [TestMethod]
        public void SetTest()
        {
            string text = @"
(define [x y] [1 2])
(set! [x y] [y x])
[x y]";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);

            Assert.AreEqual(2, ((IntValue)value[0]).Value);
            Assert.AreEqual(1, ((IntValue)value[1]).Value);
        }

        [TestMethod]
        public void SetTest1()
        {
            string text = @"
(define [x y] [1 2])
(set! [x y] [y x])";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);

            Assert.AreEqual(2, ((IntValue)value[0]).Value);
            Assert.AreEqual(1, ((IntValue)value[1]).Value);
        }

        [TestMethod]
        public void SetTest2()
        {
            string text = @"
(define [x y] [1 2])
(set! {:a x :b y} {:a 7 :b 8})
[x y]";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);

            Assert.AreEqual(7, ((IntValue)value[0]).Value);
            Assert.AreEqual(8, ((IntValue)value[1]).Value);
        }

        [TestMethod]
        public void SetTest4()
        {
            string text = @"
(define bb {:x 1 :y 2})
(set! bb.x 10)
[bb.x bb.y]";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);

            Assert.AreEqual(10, ((IntValue)value[0]).Value);
            Assert.AreEqual(2, ((IntValue)value[1]).Value);
        }

        [TestMethod]
        public void SetTest5()
        {
            string text = @"
(define aa [1 2 3])
(set! aa.0 10)
(define bb {:x 1 :y 2})
(set! bb.x 10)
[aa bb]


(define [x y] [1 2])
[x y]

(set! [x y] [y x])
[x y]

(set! {:a x :b y} {:a 7 :b 8})
[x y]


(define
  {
    :c xx
    :d [uu vv]
  }
  {
   :d [8 9]
   :c 7
  }
)

[xx uu vv]

(if (< 3 2) 10 (seq (define notthere 24) (+ 1 notthere)))

(seq (define x 19) x)

(define ok (fun (x) (set! x 10) x))
[(ok 8) x]

(define arr [1 2 3])
(define [t1 t2 t3] arr)
[t1 t2 t3]

(define [u1 [v1 v2] u2] [2 [3 5] 7])
[u1 u2 v1 v2]
";
            Value value = Interpreter.InterpText(text);
        }

        [TestMethod]
        public void ScopeTest()
        {
            string text = @"
(define x 19)

(define ok (fun (x) (set! x 10) x))
[(ok 8) x]";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);

            Assert.AreEqual(10, ((IntValue)value[0]).Value);
            Assert.AreEqual(19, ((IntValue)value[1]).Value);
        }

        [TestMethod]
        public void RecordTest()
        {
            string text = @"
(define A {:e 1 :f 2})
A";
            RecordType value = (RecordType)Interpreter.InterpText(text);
        }

        [TestMethod]
        public void AttrTest1()
        {
            string text = @"
(define x [10 20 30 40])

(define bar
  (fun ()
    (define v1 [0 1 2 3])
    (define foo (fun (i) v1.i))
    (foo 1)))

(define idx (bar))
x.idx";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(20, value.Value);
        }

        [TestMethod]
        public void AttrTest2()
        {
            string text = @"
(define mat
    [[11 12 13 14]
     [21 22 23 24]
     [31 32 33 34]
     [41 42 43 44]]
)

mat.0";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);
            Assert.AreEqual(11, ((IntValue)value[0]).Value);
        }

        [TestMethod]
        public void AttrTest3()
        {
            string text = @"
(define mat
    [[11 12 13 14]
     [21 22 23 24]
     [31 32 33 34]
     [41 42 43 44]]
)

mat.0.2";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(13, value.Value);

            text = @"
(define mat
    [[11 12 13 14]
     [21 22 23 24]
     [31 32 33 34]
     [41 42 43 44]]
)
(define x 2)
mat.0.x";
            value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(13, value.Value);
        }

        [TestMethod]
        public void AttrTest4()
        {
            string text = @"
(define mat
    [[11 12 13 14]
     [21 22 23 24]
     [31 32 33 34]
     [41 42 43 44]]
)

mat.0.2";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(13, value.Value);
        }

        [TestMethod]
        public void AttrTest5()
        {
            string text = @"
(define u 1)
(define setU
  (fun (x)
    (set! u x)))
(setU 10)
u";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(10, value.Value);
        }

        [TestMethod]
        public void AttrTest6()
        {
            string text = @"
(define ^ (fun (x y) (* x y)))
(^ 2 3.2)
";
            FloatValue value = (FloatValue)Interpreter.InterpText(text);
            Assert.AreEqual(6.4, value.Value);
        }

        [TestMethod]
        public void AttrTest7()
        {
            string text = @"
(define aa [1 2 3])
(set! aa.1 10)
aa.1";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(10, value.Value);
        }

        [TestMethod]
        public void AttrTest8()
        {
            string text = @"
(define v1 [1 2 3 4 5 6])
(define v2 [1 2 3 4 5 6])
(define i v2.1)
v1.i";
            Value value = Interpreter.InterpText(text);
        }

        [TestMethod]
        public void AttrTest9()
        {
            string text = @"
(define v1 {:a 1 :b {:c 3 :d 4}})
v1.b";
            RecordType value = (RecordType)Interpreter.InterpText(text);
            Assert.AreEqual(3, ((IntValue)value.Properties.Lookup("c")).Value);
        }

        [TestMethod]
        public void AttrTest10()
        {
            string text = @"
(define v1 {:a 1 :b {:c 3 :d 4}})
v1.b.c";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(3, value.Value);

            text = @"
(define v1 {:a 1 :b {:c 3 :d 4}})
(define x ""c"")
v1.b.x";
            value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(3, value.Value);
        }

        [TestMethod]
        public void Test2Test()
        {
            string text = "(define a 100.123456)";
            Value value = Interpreter.InterpText(text);
        }

        [TestMethod]
        public void Test3Test()
        {
            string text = "100.123456";
            Value value = Interpreter.InterpText(text);
        }

        [TestMethod]
        public void DefineFunTest()
        {
            string text = @"
(define f (fun(x) (  +      x 1     ))

    )";
            Value value = Interpreter.InterpText(text);
        }

        [TestMethod]
        public void TestTest()
        {
            string text = @"
((fun (x y)
  (+ x y)) 1 2)";
            IntValue value = (IntValue)Interpreter.InterpText(text);
            Assert.AreEqual(3, value.Value);
        }

        [TestMethod]
        public void test2Test()
        {
            string text = @"
(define
  {:x a :y b}
  {:x 1 :y 2})

(set! {:x a :y b}
      {:x 2 :y 3})
[a b]
";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);

            Assert.AreEqual(2, ((IntValue)value[0]).Value);
            Assert.AreEqual(3, ((IntValue)value[1]).Value);
        }


        [TestMethod]
        public void test4Test()
        {
            string text = "(+ 2 3)";
            Value value = Interpreter.InterpText(text);
            Assert.AreEqual(5, ((IntValue)value).Value);
        }

        [TestMethod]
        public void FunTest()
        {
            string text = @"
(define f
  (fun ([x Int]
        [y Int])
    (+ x y)))

(f :y 2 :x 1)";
            Value value = Interpreter.InterpText(text);
            text = @"
(define 我是
  (fun (x n)
    [""我是"" x n]))

(define 三 3)

(我是 ""猪头"" 三)";
            value = Interpreter.InterpText(text);
        }

        [TestMethod]
        public void FunTest2()
        {
            string text = @"
(define f
  (fun ([x Float]
        [y Int])
    (+ x y)))

(f :y 2 :x 1.0)";
            Value value = Interpreter.InterpText(text);
        }

        [TestMethod]
        public void FunTest3()
        {
            string text = @"
(define f
  (fun (x)
    (+ x.a x.b)))

(f {:a 2 :b 1.0})";
            FloatValue value = (FloatValue)Interpreter.InterpText(text);
            Assert.AreEqual(3.0, value.Value);
        }

        [TestMethod]
        public void FunTest4()
        {
            string text = @"
(define f
  (fun (x)
    (+ x.a x.b)))

(define a (f {:a 2 :b 1.0}))
a";
            FloatValue value = (FloatValue)Interpreter.InterpText(text);
            Assert.AreEqual(3.0, value.Value);
        }

        [TestMethod]
        public void TypesTest()
        {
            string text = @"
-- factorial
(define fact
  (fun ([x Int])
    (if (= x 0) 1 (* x (fact (- x 1))))))

(fact 5)


------------- mutural recursive functions -------------
(define even
  (fun ([x Int])
    (if (= x 0)
        true
        (if (= x 1)
            false
            (not (odd (- x 1)))))))

(define odd
  (fun ([x Int])
    (if (= x 0)
        false
        (if (= x 1)
            true
            (not (even (- x 1)))))))

(odd 10)


(define 我是
  (fun ([x Int] [n Int])
    [""我是"" x n]))

(define 三 3)

(我是 ""猪头"" 三)";
            Value value = Interpreter.InterpText(text);
        }


        [TestMethod]
        public void TypesTest2()
        {
            string text = @"
-- factorial
(define fact
  (fun (x Int)
    (if (= x 0) 1 (* x (fact (- x 1))))))

(fact 5)


------------- mutural recursive functions -------------
(define even
  (fun (x Int)
    (if (= x 0)
        true
        (if (= x 1)
            false
            (not (odd (- x 1)))))))

(define odd
  (fun (x Int)
    (if (= x 0)
        false
        (if (= x 1)
            true
            (not (even (- x 1)))))))

(odd 10)";
            Value value = Interpreter.InterpText(text);
        }

        [TestMethod]
        public void TypesTest3()
        {
            string text = @"
(define f
  (fun ([x Int] [y Bool] [-> String])
    [x y]))

(f :x 1 :y false)
(f 3 5)";
            Value value = Interpreter.InterpText(text);
        }

        [TestMethod]
        public void TypesTest4()
        {
            string text = @"
{:a 1 :b 2}
{:a Int :b Float}";
            Value value = Interpreter.InterpText(text);
        }

        [TestMethod]
        public void TypesTest5()
        {
            string text = @"
(define list-of
  (fun (T n)
    (if (= n 1)
        {:first T}
        {:first T :rest (list-of T (- n 1))}
    )
  )
)

(list-of Int 5)";
            RecordType value = (RecordType)Interpreter.InterpText(text);
        }

        [TestMethod]
        public void CharTest1()
        {
            string text = @"
(define a 'a')
(define b '\'')
[a b 'c' '\'' 'd']";
            VectorValue value = (VectorValue)Interpreter.InterpText(text);
            Assert.AreEqual('a', ((CharValue)value[0]).Value);
            Assert.AreEqual('\'', ((CharValue)value[1]).Value);
            Assert.AreEqual('c', ((CharValue)value[2]).Value);
            Assert.AreEqual('\'', ((CharValue)value[3]).Value);
            Assert.AreEqual('d', ((CharValue)value[4]).Value);
        }

        [TestMethod]
        public void CharTest2()
        {
            string text = @"
(define c '\'')
c";
            CharValue value = (CharValue)Interpreter.InterpText(text);
            Assert.AreEqual('\'', value.Value);
        }
    }
}
