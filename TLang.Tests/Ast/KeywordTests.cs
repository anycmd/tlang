﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TLang.Ast;

namespace TLang.Tests.Ast
{
    [TestClass]
    public class KeywordTests
    {
        [TestMethod]
        public void KeywordToStringTest()
        {
            KeywordNode keyword = new KeywordNode("x", null, 0, 0, 0, 0);
            Assert.AreEqual(":x", keyword.ToString());
        }
    }
}
