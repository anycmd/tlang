﻿using System;
using System.Collections.Generic;

namespace TLang.Ast
{
    using Values;

    public class FunNode : Node
    {
        public readonly List<NameNode> Prams;
        public readonly Node Body;
        private readonly Scope _propertyForm;


        public FunNode(List<NameNode> prams, Scope propertyForm, Node body, String file, int start, int end, int line, int col) : base(file, start, end, line, col)
        {
            this.Prams = prams;
            this._propertyForm = propertyForm;     // unevaluated property form
            this.Body = body;
        }


        public override Value Interp(Scope s)
        {
            // evaluate and cache the properties in the closure
            Scope properties = _propertyForm == null ? null : DeclareNode.EvalProperties(_propertyForm, s);
            return new Closure(this, properties, s);
        }

        public override String ToString()
        {
            return "(" + Constants.FunKeyword + " (" + Prams + ") " + Body + ")";
        }
    }
}
