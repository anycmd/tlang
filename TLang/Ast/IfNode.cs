﻿using System;

namespace TLang.Ast
{
    using Values;

    public class IfNode : Node
    {
        private readonly Node _test;
        private readonly Node _then;
        private readonly Node _orelse;


        public IfNode(Node test, Node then, Node orelse, String file, int start, int end, int line, int col) : base(file, start, end, line, col)
        {
            this._test = test;
            this._then = then;
            this._orelse = orelse;
        }
        
        public override Value Interp(Scope s)
        {
            BoolValue tv = Interp(_test, s) as BoolValue;
            if (tv == null)
            {
                throw Util.GeneralError("test result is not BoolValue type");
            }
            if (tv.Value)
            {
                return Interp(_then, s);
            }
            else
            {
                return Interp(_orelse, s);
            }
        }

        public override String ToString()
        {
            return "(" + Constants.IfKeyword + " " + _test + " " + _then + " " + _orelse + ")";
        }
    }
}
