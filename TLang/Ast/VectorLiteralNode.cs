﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TLang.Ast
{
    using Values;

    public class VectorLiteralNode : Node
    {

        public readonly List<Node> Elements;
        // TODO:types


        public VectorLiteralNode(List<Node> elements, String file, int start, int end, int line, int col) : base(file, start, end, line, col)
        {
            this.Elements = elements;
        }

        public override Value Interp(Scope s)
        {
            return new VectorValue(InterpList(Elements, s));
        }

        public override string ToString()
        {
            return Constants.SquareBegin + string.Join(" ", Elements.Select(a=>a.ToString()) + Constants.SquareEnd);
        }
    }
}
