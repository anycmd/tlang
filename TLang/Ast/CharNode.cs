﻿using System;
using TLang.Values;

namespace TLang.Ast
{
    public class CharNode : Node
    {
        public readonly char Value;


        public CharNode(char value, String file, int start, int end, int line, int col) : base(file, start, end, line, col)
        {
            this.Value = value;
        }


        public override Value Interp(Scope s)
        {
            return new CharValue(Value);
        }

        public override String ToString()
        {
            return "'" + Value + "'";
        }
    }
}
