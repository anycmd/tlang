﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TLang.Ast
{
    using Values;

    public class RecordLiteralNode : Node
    {

        public readonly Map<String, Node> Map = new Map<string, Node>();
        // TODO:type map

        public RecordLiteralNode(List<Node> contents, String file, int start, int end, int line, int col) : base(file, start, end, line, col)
        {
            if (contents.Count % 2 != 0)
            {
                throw Util.GeneralError(this, "record initializer must have even number of elements");
            }

            for (int i = 0; i < contents.Count; i += 2)
            {
                Node key = contents[i];
                Node value = contents[i + 1];
                if (key is KeywordNode)
                {
                    if (value is KeywordNode)
                    {
                        throw Util.GeneralError(value, "keywords shouldn't be used as values: " + value);
                    }
                    else
                    {
                        Map.Put(((KeywordNode)key).Id, value);
                    }
                }
                else
                {
                    throw Util.GeneralError(key, "record initializer key is not a keyword: " + key);
                }
            }
        }


        public override Value Interp(Scope s)
        {
            Scope properties = new Scope();
            foreach (var e in Map)
            {
                properties.PutValue(e.Key, e.Value.Interp(s));
            }
            return new RecordType(null, this, properties);
        }

        public override String ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Constants.SquareBegin);
            bool first = true;
            foreach (var e in Map)
            {
                if (!first)
                {
                    sb.Append(" ");
                }
                sb.Append(Constants.ColonChar + e.Key + " " + e.Value);
                first = false;
            }
            sb.Append(Constants.SquareEnd);
            return sb.ToString();
        }
    }
}
