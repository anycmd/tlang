﻿using System;

namespace TLang.Ast
{
    using Values;

    public class NameNode : Node
    {
        public readonly String Id;


        public NameNode(String id, String file, int start, int end, int line, int col) : base(file, start, end, line, col)
        {
            this.Id = id;
        }

        public override Value Interp(Scope s)
        {
            Value v = s.Lookup(Id);
            if (v != null)
            {
                return v;
            }
            else
            {
                throw Util.GeneralError(this, "unbound variable: " + Id);
            }
        }

        public override String ToString()
        {
            return Id;
        }
    }
}
