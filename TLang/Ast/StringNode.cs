﻿using System;

namespace TLang.Ast
{
    using Values;

    public class StringNode : Node
    {
        public readonly String Value;


        public StringNode(String value, String file, int start, int end, int line, int col) : base(file, start, end, line, col)
        {
            this.Value = value;
        }


        public override Value Interp(Scope s)
        {
            return new StringValue(Value);
        }

        public override String ToString()
        {
            return "\"" + Value + "\"";
        }
    }
}
