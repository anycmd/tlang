﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TLang.Parsers
{
    using Ast;
    using Values;

    public class TupleNode : Node
    {
        public readonly List<Node> Elements;
        public readonly Node Open;
        public readonly Node Close;


        public TupleNode(List<Node> elements, Node open, Node close, String file, int start, int end, int line, int col) : base(file, start, end, line, col)
        {
            this.Elements = elements;
            this.Open = open;
            this.Close = close;
        }


        public Node GetHead()
        {
            if (Elements.IsEmpty())
            {
                return null;
            }
            else
            {
                return Elements[0];
            }
        }


        public override Value Interp(Scope s)
        {
            throw new NotSupportedException();
        }

        public override String ToString()
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < Elements.Count; i++)
            {
                sb.Append(Elements[i].ToString());
                if (i != Elements.Count - 1)
                {
                    sb.Append(" ");
                }
            }

            return (Open == null ? "" : Open.ToString()) + sb.ToString() + (Close == null ? "" : Close.ToString());
        }
    }
}
