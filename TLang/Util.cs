﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace TLang
{
    using Ast;

    public class Util
    {
        public static String ReadFile(String path)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }
            try
            {
                byte[] encoded = File.ReadAllBytes(Path.GetFullPath(path));
                return Encoding.UTF8.GetString(encoded);
            }
            catch (IOException)
            {
                return null;
            }
        }

        public static void Msg(String m)
        {
            Console.WriteLine(m);
        }


        public static GeneralError GeneralError(String msg)
        {
            Console.WriteLine(msg);
            return new GeneralError(msg);
        }


        public static GeneralError GeneralError(Node loc, String msg)
        {
            msg = loc.GetFileLineCol() + " " + msg;
            Console.WriteLine(msg);
            return new GeneralError(loc, msg);
        }

        public static String JoinWithSep(ICollection<Object> ls, String sep)
        {
            if (ls == null)
            {
                throw new ArgumentNullException(nameof(ls));
            }
            StringBuilder sb = new StringBuilder();
            int i = 0;
            foreach (Object s in ls)
            {
                if (i > 0)
                {
                    sb.Append(sep);
                }
                sb.Append(s.ToString());
                i++;
            }
            return sb.ToString();
        }
    }
}
