﻿namespace TLang
{
    using Ast;
    using Values;
    using Parsers;

    public class Interpreter
    {
        public static Value InterpText(string text)
        {
            Node program = Parser.Parse(text, null);
            return program.Interp(Scope.BuildInitScope());
        }

        public static Value InterpFile(string file)
        {
            Node program = Parser.Parse(null, file);
            return program.Interp(Scope.BuildInitScope());
        }
    }
}
