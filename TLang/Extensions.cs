﻿using System.Collections.Generic;

namespace TLang
{
    public static class Extensions
    {
        public static bool IsEmpty<T>(this IList<T> list)
        {
            return list.Count == 0;
        }
    }

    public class Map<TKey, TValue> : Dictionary<TKey, TValue>
    {
        public TValue Get(TKey key)
        {
            this.TryGetValue(key, out TValue value);

            return value;
        }

        public void Put(TKey key, TValue value)
        {
            if (this.ContainsKey(key))
            {
                this[key] = value;
            }
            else
            {
                this.Add(key, value);
            }
        }

        public void PutAll(Map<TKey, TValue> items)
        {
            foreach (var item in items)
            {
                this.Add(item.Key, item.Value);
            }
        }

        public bool IsEmpty()
        {
            return this.Count == 0;
        }
    }
}
