﻿
namespace TLang.Values
{
    using Ast;

    public class FunType : TType
    {
        public readonly new FunNode Fun;
        public readonly Scope Properties;
        public readonly Scope Env;


        public FunType(FunNode fun, Scope properties, Scope env) : base("Fun")
        {
            this.Fun = fun;
            this.Properties = properties;
            this.Env = env;
        }


        public override string ToString()
        {
            return Properties.ToString();
        }
    }
}
