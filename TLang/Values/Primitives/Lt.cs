using System.Collections.Generic;

namespace TLang.Values.Primitives
{
    using Ast;

    public class Lt : PrimFun
    {
        public Lt() : base("<", 2)
        {
            
        }

        public override Value Apply(List<Value> args, Node location)
        {

            Value v1 = args[0];
            Value v2 = args[1];
            if (v1 is IntValue && v2 is IntValue)
            {
                return BoolValue.Of(((IntValue)v1).Value < ((IntValue)v2).Value);
            }
            if (v1 is FloatValue && v2 is FloatValue)
            {
                return BoolValue.Of(((FloatValue)v1).Value < ((FloatValue)v2).Value);
            }
            if (v1 is FloatValue && v2 is IntValue)
            {
                return BoolValue.Of(((FloatValue)v1).Value < ((IntValue)v2).Value);
            }
            if (v1 is IntValue && v2 is FloatValue)
            {
                return BoolValue.Of(((IntValue)v1).Value < ((FloatValue)v2).Value);
            }

            throw Util.GeneralError(location, "incorrect argument types for <: " + v1 + ", " + v2);
        }
    }
}