﻿using System;
using System.Collections.Generic;
using TLang.Ast;

namespace TLang.Values.Primitives
{
    public class Pow : PrimFun
    {
        public Pow() : base("^", 2)
        {

        }

        public override Value Apply(List<Value> args, Node location)
        {
            Value v1 = args[0];
            Value v2 = args[1];
            if (v1 is IntValue a1 && v2 is IntValue b1)
            {
                return new FloatValue(Math.Pow(a1.Value, b1.Value));
            }
            if (v1 is FloatValue a2 && v2 is FloatValue b2)
            {
                return new FloatValue(Math.Pow(a2.Value, b2.Value));
            }
            if (v1 is FloatValue a3 && v2 is IntValue b3)
            {
                return new FloatValue(Math.Pow(a3.Value, b3.Value));
            }
            if (v1 is IntValue a4 && v2 is FloatValue b4)
            {
                return new FloatValue(Math.Pow(a4.Value, b4.Value));
            }
            throw Util.GeneralError(location, "incorrect argument types for +: " + v1 + ", " + v2);
        }
    }
}
