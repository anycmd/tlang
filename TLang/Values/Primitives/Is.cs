﻿using System.Collections.Generic;
using TLang.Ast;

namespace TLang.Values.Primitives
{
    public class Is : PrimFun
    {
        public Is() : base("is?", 2)
        {

        }

        public override Value Apply(List<Value> args, Node location)
        {
            Value v1 = args[0];
            Value v2 = args[1];
            if (v1 == TType.Int && v2 is IntValue)
            {
                return BoolValue.True;
            }
            if (v1 == TType.Float && v2 is FloatValue)
            {
                return BoolValue.True;
            }
            if (v1 == TType.Bool && v2 is BoolValue)
            {
                return BoolValue.True;
            }
            if (v1 == TType.String && v2 is StringValue)
            {
                return BoolValue.True;
            }
            if (v1 == TType.Vector && v2 is VectorValue)
            {
                return BoolValue.True;
            }
            // TODO:RecordType
            // TODO:FunType
            if (v1 == TType.Any)
            {
                return BoolValue.True;
            }
            return BoolValue.False;
        }
    }
}
