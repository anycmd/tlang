﻿using System.Collections.Generic;
using TLang.Ast;

namespace TLang.Values.Primitives
{
    public class NotEq : PrimFun
    {
        public NotEq() : base("!=", 2)
        {

        }

        public override Value Apply(List<Value> args, Node location)
        {
            BoolValue eq = Eq.IsEq(args, location) as BoolValue;
            if (eq == null)
            {
                return null;
            }
            return BoolValue.Of(!eq.Value);
        }
    }
}
