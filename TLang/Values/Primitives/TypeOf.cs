﻿using System.Collections.Generic;
using TLang.Ast;

namespace TLang.Values.Primitives
{
    public class TypeOf : PrimFun
    {
        public TypeOf() : base("typeof", 1)
        {

        }

        public override Value Apply(List<Value> args, Node location)
        {
            return args[0].Type;
        }
    }
}
