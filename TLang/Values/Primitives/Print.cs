﻿using System;
using System.Collections.Generic;

namespace TLang.Values.Primitives
{
    using Ast;

    public class Print : PrimFun
    {

        public Print() : base("print", 1)
        {
        }

        public override Value Apply(List<Value> args, Node location)
        {
            bool first = true;
            foreach (Value v in args)
            {
                if (!first)
                {
                    Console.Write(", ");
                }
                Console.Write(v);
                first = false;
            }
            Console.WriteLine();
            return VoidValue.Void;
        }
    }
}
