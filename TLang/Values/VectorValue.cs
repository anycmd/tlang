﻿using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace TLang.Values
{
    public class VectorValue : Value, IEnumerable<Value>
    {
        private readonly List<Value> _values;
        
        public VectorValue(List<Value> values) : base(TType.Vector)
        {
            this._values = values;
        }

        public Value this[int idx]
        {
            get { return this._values[idx]; }
            set { this._values[idx] = value; }
        }

        public Value Append(Value value)
        {
            this._values.Add(value);

            return value;
        }

        public Value RemoveAt(int idx)
        {
            Value value = this._values[idx];
            this.RemoveAt(idx);
            return value;
        }

        public int Count
        {
            get { return this._values.Count; }
        }

        public IEnumerator<Value> GetEnumerator()
        {
            return this._values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this._values.GetEnumerator();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Constants.SquareBegin);

            bool first = true;
            foreach (Value v in _values)
            {
                if (!first)
                {
                    sb.Append(" ");
                }
                sb.Append(v);
                first = false;
            }

            sb.Append(Constants.SquareEnd);

            return sb.ToString();
        }
    }
}
