﻿namespace TLang.Values
{
    public class VoidValue : Value
    {
        public static readonly VoidValue Void = new VoidValue();

        private VoidValue() : base(TType.Void) { }

        public override string ToString()
        {
            return "void";
        }
    }
}
