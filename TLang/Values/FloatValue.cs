﻿namespace TLang.Values
{
    public class FloatValue : Value
    {
        public readonly double Value;

        public FloatValue(double value) : base(TType.Float)
        {
            this.Value = value;
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}
