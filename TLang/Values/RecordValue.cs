﻿using System;
using System.Text;

namespace TLang.Values
{
    public class RecordValue : Value
    {
        private readonly String _name;
        private readonly RecordType _type;
        public readonly Scope Properties;


        public RecordValue(String name, RecordType type, Scope properties) : base(TType.Record)
        {
            this._name = name;
            this._type = type;
            this.Properties = properties;
        }


        public override String ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Constants.ParenBegin);
            sb.Append(Constants.RecordKeyword).Append(" ");
            sb.Append(_name == null ? "_" : _name);

            foreach (string field in Properties.KeysLocal)
            {
                sb.Append(" ").Append(Constants.SquareBegin);
                sb.Append(field).Append(" ");
                sb.Append(Properties.LookupLocal(field));
                sb.Append(Constants.SquareEnd);
            }

            sb.Append(Constants.ParenEnd);

            return sb.ToString();
        }
    }
}
