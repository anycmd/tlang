﻿namespace TLang.Values
{
    public class BoolValue : Value
    {
        public static readonly BoolValue True = new BoolValue(true);
        public static readonly BoolValue False = new BoolValue(false);

        public readonly bool Value;

        private BoolValue(bool value) : base(TType.Bool)
        {
            this.Value = value;
        }

        public static BoolValue Of(bool value)
        {
            return value ? True : False;
        }

        public override string ToString()
        {
            return Value ? "true" : "false";
        }
    }
}
