﻿namespace TLang.Values
{
    public class CharValue : Value
    {
        public readonly char Value;

        public CharValue(char value) : base(TType.String)
        {
            this.Value = value;
        }

        public override string ToString()
        {
            return $"'{Value}'";
        }
    }
}
