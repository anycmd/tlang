﻿
namespace TLang.Values
{
    using Ast;

    public class Closure : Value
    {
        public readonly FunNode Fun;
        public readonly Scope Properties;
        public readonly Scope Env;

        public Closure(FunNode fun, Scope properties, Scope env) : base(TType.Fun)
        {
            this.Fun = fun;
            this.Properties = properties;
            this.Env = env;
        }
        
        public override string ToString()
        {
            return Fun.ToString();
        }
    }
}
