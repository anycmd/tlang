﻿using System;

namespace TLang
{
    using Ast;


    public class GeneralError : Exception
    {
        private readonly Node _location;


        public GeneralError(Node location, String msg) : base(msg)
        {
            this._location = location;
        }


        public GeneralError(String msg) : base(msg)
        {
        }


        public override String ToString()
        {
            if (_location != null)
            {
                return _location.GetFileLineCol() + ": " + base.Message;
            }
            else
            {
                return base.Message;
            }
        }
    }
}
